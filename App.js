import React, { Component } from "react";
import Text from "./robotoText";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  TouchableOpacity
} from "react-native";

export default class App extends Component {
  state = {
    counter: 0,
    match: undefined,
    loadNext: false
  };

  componentDidMount() {
    this.fetchNext();
  }

  fetchNext = () =>
    fetch("https://randomuser.me/api")
      .then(r => r.json())
      .then(({ results: [{ name, dob, picture: { thumbnail } }] }) => {
        this.setState({
          match: {
            name: getName(name),
            age: dob.age,
            image: thumbnail
          },
          loadNext: false
        });
      });

  componentDidUpdate(prevProps, prevState) {
    if (prevState.loadNext === false && this.state.loadNext) {
      this.fetchNext();
    }
  }

  userItem = ({ name, age, image }) => [
    <Image
      style={{ width: 128, height: 128, borderRadius: 64 }}
      key="userImg"
      source={{
        uri: image
      }}
    />,
    <Text style={{ fontSize: 32 }} key="name">
      {name}
    </Text>,
    <Text style={{ fontSize: 14 }} key="age">{`(${age})`}</Text>
  ];

  render() {
    let { counter, match, loadNext } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>Gender Neutral Dating App</Text>
          <View
            style={{
              alignItems: "center",
              backgroundColor: counter >= 5 ? "#FF6F00" : "white",
              paddingHorizontal: 8,
              paddingVertical: 4,
              borderRadius: 4,
              marginRight: 16,
              fontSize: 14
            }}
          >
            <Text styl>{counter}</Text>
          </View>
        </View>
        <View style={styles.center}>
          {!loadNext && match ? (
            this.userItem(match)
          ) : (
            <Text style={{ fontSize: 32 }}>Loading...</Text>
          )}
        </View>
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity
            style={{
              flex: 1,
              backgroundColor: "#424242",
              justifyContent: "center",
              height: 60
            }}
            disabled={loadNext}
            onPress={() => this.setState({ loadNext: true })}
          >
            <Text
              style={{
                ...styles.buttons,
                color: loadNext ? "#757575" : "white"
              }}
            >
              No
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flex: 1,
              backgroundColor: loadNext || counter >= 5 ? "#424242" : "#FF6F00",
              height: 60,
              justifyContent: "center"
            }}
            onPress={() =>
              this.setState({ loadNext: true, counter: counter + 1 })
            }
            disabled={loadNext || counter >= 5}
          >
            <Text
              style={{
                ...styles.buttons,
                color: loadNext || counter >= 5 ? "#757575" : "white"
              }}
            >
              Yes
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "space-between",
    marginTop: Platform.select({ ios: 18, android: 0 }),
    flex: 1
  },
  buttons: {
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 18
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#424242",
    alignItems: "center",
    height: 60,
    paddingHorizontal: 5
  },
  headerText: {
    fontSize: 18,
    color: "white",
    fontWeight: "bold"
  },
  center: {
    alignItems: "center"
  }
});

const capitalize = str => {
  return (
    str[0].toUpperCase() +
    str
      .split("")
      .slice(1)
      .join("")
  );
};

const getName = ({ first, last }) => `${capitalize(first)} ${capitalize(last)}`;
